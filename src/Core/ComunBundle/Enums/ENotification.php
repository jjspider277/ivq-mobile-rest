<?php 
 namespace Core\ComunBundle\Enums;
 class ENotification{
	 const 	JOINED_GROUP = 1;
	 const 	LIKE_PHOTO = 2;
	 const 	STARTED_FOLLOWING_YOU = 3;
	 const 	SHARED_YOUR_PHOTO = 4;
	 const 	ATTACHED_MEDIA_TO_YOUR_EVENT = 5;
	 const 	ATTEND_TO_EVENT = 6;
	 const 	SHARE_B_CARD = 7;
} 
 ?>